package com.devcamp.customerorderproductcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderproductcrud.model.Order;

public interface OrderRepo extends JpaRepository<Order, Integer> {

}
