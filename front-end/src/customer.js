$(document).ready(function () {
    var vTable = $('#customer-table').DataTable({
        "ajax": {
            "url": "http://localhost:8080/customers",
            "dataSrc": ""
        },
        "columns": [
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            { "data": "lastName" },
            { "data": "firstName" },
            { "data": "phoneNumber" },
            { "data": "address" },
            { "data": "city" },
            { "data": "state" },
            { "data": "postalCode" },
            { "data": "country" },
            { "data": "salesRepEmployeeNumber" },
            { "data": "creditLimit" },
            {
                "data": null,
                "render": function (data, type, row) {
                    return '<button class="btn btn-primary btn-edit" data-id="' + row.id + '">' +
                        '<i class="fas fa-edit"></i>' +
                        '</button>' +
                        '<button class="btn btn-danger btn-delete" data-id="' + row.id + '">' +
                        '<i class="fas fa-trash-alt"></i>' +
                        '</button>';
                }
            }
        ]
    });

    $('#btn-add-new-customer').click(function () {
        $('#customer-create-modal').modal('show');
    });

    $('#btn-create-modal').click(function () {
        var vNewCustomer = {
            lastName: $('#inp-create-lastname').val(),
            firstName: $('#inp-create-firstname').val(),
            phoneNumber: $('#inp-create-phone').val(),
            address: $('#inp-create-address').val(),
            city: $('#inp-create-city').val(),
            state: $('#inp-create-state').val(),
            postalCode: $('#inp-create-postal-code').val(),
            country: $('#inp-create-country').val(),
            salesRepEmployeeNumber: parseInt($('#inp-create-sales').val()),
            creditLimit: parseInt($('#inp-create-credit-limit').val())
        };

        $.ajax({
            type: "POST",
            url: "http://localhost:8080/customers",
            data: JSON.stringify(vNewCustomer),
            contentType: "application/json",
            success: function (data) {
                vTable.row.add(data).draw(false);
                $('#customer-create-modal').modal('hide');
                $('#createForm')[0].reset();
                vTable.clear().draw();
                vTable.ajax.reload(null, false);
            }
        });
    });

    $(document).on('click', '.btn-edit', function () {
        var vCustomerId = $(this).data('id');

        $.ajax({
            type: "GET",
            url: "http://localhost:8080/customers/" + vCustomerId,
            success: function (data) {
                $('#inp-update-lastname').val(data.lastName);
                $('#inp-update-firstname').val(data.firstName);
                $('#inp-update-phone').val(data.phoneNumber);
                $('#inp-update-address').val(data.address);
                $('#inp-update-city').val(data.city);
                $('#inp-update-state').val(data.state);
                $('#inp-update-postal-code').val(data.postalCode);
                $('#inp-update-country').val(data.country);
                $('#inp-update-sales').val(data.salesRepEmployeeNumber);
                $('#inp-update-credit-limit').val(data.creditLimit);
                $('#customer-update-modal').modal('show');
            }
        });
    });

    $('#btn-update-modal').click(function () {
        var vCustomerId = $('.btn-edit').data('id');

        var vUpdatedCustomer = {
            lastName: $('#inp-update-lastname').val(),
            firstName: $('#inp-update-firstname').val(),
            phoneNumber: $('#inp-update-phone').val(),
            address: $('#inp-update-address').val(),
            city: $('#inp-update-city').val(),
            state: $('#inp-update-state').val(),
            postalCode: $('#inp-update-postal-code').val(),
            country: $('#inp-update-country').val(),
            salesRepEmployeeNumber: parseInt($('#inp-update-sales').val()),
            creditLimit: parseInt($('#inp-update-credit-limit').val())
        };

        $.ajax({
            type: "PUT",
            url: "http://localhost:8080/customers/" + vCustomerId,
            data: JSON.stringify(vUpdatedCustomer),
            contentType: "application/json",
            success: function (data) {
                var rowIndex = vTable.row('.selected').index();
                vTable.row(rowIndex).data(data).draw(false);
                $('#customer-update-modal').modal('hide');
                vTable.clear().draw();
                vTable.ajax.reload(null, false);
            }
        });
    });

    $(document).on('click', '.btn-delete', function () {
        var vCustomerId = $(this).data('id');
        $('#customer-delete-modal').modal('show');
        $('#customer-delete-modal').data('customerId', vCustomerId);
    });

    $('#btn-delete-modal').click(function () {
        var vCustomerId = $('#customer-delete-modal').data('customerId');

        $.ajax({
            type: "DELETE",
            url: "http://localhost:8080/customers/" + vCustomerId,
            success: function () {
                vTable.row('.selected').remove().draw(false);
                $('#customer-delete-modal').modal('hide');
                vTable.clear().draw();
                vTable.ajax.reload(null, false);
            }
        });
    });
});